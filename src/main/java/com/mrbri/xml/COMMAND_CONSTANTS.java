package com.mrbri.xml;


public final class COMMAND_CONSTANTS {
    public static final String ROOT_NODE_NAME = "node";
    public static final String NAME_NODE_NAME = "name";

    public static final String CHILD_NODES_CONTAINER_NODE_NAME = "children";
    public static final String CHILD_NODE_NAME = "child";

    public static final String IS_FILE_ATTR_NAME = "is-file";
    public static final String ROOT_IS_FILE_EXCEPTION = "\"FileSystem root node is a file \"";

    public static final String DIRECTORY_SEPARATOR = "/";
    public static final String NULL_STR = "";

    public static final String PARSE_ERROR = "Parse ERROR";
    public static final String FILE_NOT_FOUNT_EXCEPTION = "FILE not fount";

    public static final String FILE_FIELD_DESCRIPTION = "Specifies the file path to parse";
    public static final String SEARCH_FIELD_DESCRIPTION = "Specifies the file path to parse";
    public static final String ADVANCED_SEARCH_FIELD_DESCRIPTION = "Specifies extended filename search through RegularExpression";
}
