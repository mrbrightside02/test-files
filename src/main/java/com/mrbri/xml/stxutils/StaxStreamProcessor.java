package com.mrbri.xml.stxutils;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import java.io.InputStream;
import javax.xml.stream.XMLEventReader;


public class StaxStreamProcessor implements AutoCloseable {

    private static final XMLInputFactory FACTORY = XMLInputFactory.newInstance();


    private final XMLEventReader reader;

    public StaxStreamProcessor(InputStream is)
            throws XMLStreamException {
        reader = FACTORY.createXMLEventReader(is);
    }


    public XMLEventReader getReader() {
        return reader;
    }


    @Override
    public void close() {
        if (reader != null) {
            try {
                reader.close();
            } catch (XMLStreamException e) {
            }
        }
    }

}
