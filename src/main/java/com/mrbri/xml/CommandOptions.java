package com.mrbri.xml;

import com.google.devtools.common.options.Option;
import com.google.devtools.common.options.OptionsBase;


public class CommandOptions extends OptionsBase {

    @Option(
            name = "file",
            abbrev = 'f',
            help = COMMAND_CONSTANTS.FILE_FIELD_DESCRIPTION,
            defaultValue = ""
    )

    public String file;

    @Option(
            name = "search",
            abbrev = 's',
            help = COMMAND_CONSTANTS.SEARCH_FIELD_DESCRIPTION,
            defaultValue = ""
    )

    public String search;

    @Option(
            name = "search_extended",
            abbrev = 'S',
            help = COMMAND_CONSTANTS.ADVANCED_SEARCH_FIELD_DESCRIPTION,
            defaultValue = ""
    )

    public String searchExtended;


}

