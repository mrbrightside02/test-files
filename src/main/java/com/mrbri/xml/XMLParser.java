package com.mrbri.xml;

import com.mrbri.xml.filter.FilteredStream;
import com.mrbri.xml.helpers.XMLPayloadHelper;
import com.mrbri.xml.stxutils.StaxStreamProcessor;
import com.mrbri.xml.exceptions.XMLParseException;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.FileInputStream;


import java.io.FileNotFoundException;


public class XMLParser {

    private XMLPayloadHelper xmlHelper;
    private FilteredStream output;


    public XMLParser(String filename, FilteredStream output) {
        this.xmlHelper = new XMLPayloadHelper();
        this.output = output;

        this.parseFileStream(filename);
    }

    private void parseFileStream(String filename) {

        this.xmlHelper = new XMLPayloadHelper();

        try (StaxStreamProcessor processor = new StaxStreamProcessor(new FileInputStream(filename))) {
            XMLEventReader reader = processor.getReader();

            while (reader.hasNext()) {
                XMLEvent xmlEvent = reader.nextEvent();

                if (xmlEvent.isStartElement()) {
                    StartElement startElement = xmlEvent.asStartElement();
                    if (this.isRootElementOpened(startElement))
                        this.parseRootElement(startElement, reader);
                    else if (this.isChildElementOpened(startElement))
                        this.parseChildElement(startElement, reader);
                } else if (xmlEvent.isEndElement()) {
                    EndElement element = xmlEvent.asEndElement();
                    if (this.isChildContainerClosed(element)) {
                        this.xmlHelper.closeDir();
                    } else if (element.getName().getLocalPart().equals(COMMAND_CONSTANTS.CHILD_NODE_NAME)) {
                        output.filter(this.xmlHelper.getLastFile());
                    }
                }
            }
        } catch (FileNotFoundException exc) {
            System.err.println(COMMAND_CONSTANTS.FILE_NOT_FOUNT_EXCEPTION);
        } catch (XMLParseException | XMLStreamException exc) {
            System.err.println(COMMAND_CONSTANTS.PARSE_ERROR);
        }

    }


    private boolean isRootElementOpened(StartElement element) {
        return element.getName().getLocalPart().equals(COMMAND_CONSTANTS.ROOT_NODE_NAME);
    }

    private boolean isChildElementOpened(StartElement element) {
        return element.getName().getLocalPart().equals(COMMAND_CONSTANTS.CHILD_NODE_NAME);
    }

    private boolean isNameTag(StartElement element) {
        return COMMAND_CONSTANTS.NAME_NODE_NAME.equals(element.getName().getLocalPart());
    }

    private boolean isChildContainerClosed(EndElement element) {
        return element.getName().getLocalPart().equals(COMMAND_CONSTANTS.CHILD_NODES_CONTAINER_NODE_NAME);
    }


    private void parseRootElement(StartElement element, XMLEventReader reader) throws XMLParseException, XMLStreamException {
        boolean isFile = Boolean.parseBoolean(this.getIsFileAttribute(element));

        if (isFile) {
            throw new XMLParseException(COMMAND_CONSTANTS.ROOT_IS_FILE_EXCEPTION);
        }

        XMLEvent event;
        event = this.gotoNexTag(reader);
        StartElement startElement = event.asStartElement();

        if (this.isNameTag(startElement)) {
            event = reader.nextEvent();
            this.xmlHelper.setRoot(event.asCharacters().getData());
        }


    }

    private void parseChildElement(StartElement element, XMLEventReader reader) throws XMLStreamException {
        boolean isFile = Boolean.parseBoolean(this.getIsFileAttribute(element));
        XMLEvent event;
        this.gotoNexTag(reader);
        event = reader.nextEvent();

        if (!isFile) {
            this.xmlHelper.openDir(event.asCharacters().getData());
        } else {
            this.xmlHelper.setLastFile(event.asCharacters().getData());
        }
    }

    private String getIsFileAttribute(StartElement element) {
        Attribute attr = element.getAttributeByName(new QName(COMMAND_CONSTANTS.IS_FILE_ATTR_NAME));
        return attr.getValue();
    }


    private XMLEvent gotoNexTag(XMLEventReader reader) throws XMLStreamException {
        XMLEvent event;
        do {
            event = reader.nextEvent();
        }
        while (event.getEventType() != XMLStreamConstants.START_ELEMENT);
        return event;
    }
}



