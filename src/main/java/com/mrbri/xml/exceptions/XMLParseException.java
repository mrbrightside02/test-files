package com.mrbri.xml.exceptions;

public class XMLParseException extends Exception {
    private final String message;

    public XMLParseException(String message) {
        super(message);
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
