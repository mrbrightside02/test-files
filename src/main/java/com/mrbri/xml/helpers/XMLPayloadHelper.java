package com.mrbri.xml.helpers;

import com.mrbri.xml.COMMAND_CONSTANTS;

import java.util.Arrays;

public class XMLPayloadHelper {

    private String path;
    private String lastFile;

    public void setRoot(String name) {
        this.path = name;
    }

    public void setLastFile(String filename) {
        this.lastFile = filename;
    }

    public String getLastFile() {
        return this.path + this.lastFile;
    }

    public void openDir(String dirname) {
        this.path += dirname + COMMAND_CONSTANTS.DIRECTORY_SEPARATOR;
    }

    public void closeDir() {
        if (this.path != null && this.path.length() > 1) {
            String[] tokens = this.path.split(COMMAND_CONSTANTS.DIRECTORY_SEPARATOR);
            tokens = Arrays.<String>copyOf(tokens, tokens.length - 1);
            this.path = String.join(COMMAND_CONSTANTS.DIRECTORY_SEPARATOR, tokens) + COMMAND_CONSTANTS.DIRECTORY_SEPARATOR;

        }
    }
}
