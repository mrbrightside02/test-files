package com.mrbri.xml.filter;

import java.io.PrintStream;

public class PlainFilter extends FilteredStream {
    public PlainFilter(PrintStream out) {
        super(out);
    }

    @Override
    public void filter(String string) {
        this.out.println(string);
    }
}
