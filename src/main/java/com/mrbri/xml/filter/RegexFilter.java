package com.mrbri.xml.filter;

import com.mrbri.xml.COMMAND_CONSTANTS;

import java.io.PrintStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexFilter extends FilteredStream {

    private Pattern pattern;
    private Matcher matcher;

    public RegexFilter(PrintStream out) {
        super(out);
    }

    public void setPattern(String pattern) {
        pattern = "^" + pattern.replace("`", COMMAND_CONSTANTS.NULL_STR) + "$";
        this.pattern = Pattern.compile(pattern);
    }

    @Override
    public void filter(String string) {
        String[] tokens = string.split(COMMAND_CONSTANTS.DIRECTORY_SEPARATOR);
        this.matcher = this.pattern.matcher(tokens[tokens.length - 1]);
        if (matcher.matches()) {
            this.out.println(string);
        }
    }
}
