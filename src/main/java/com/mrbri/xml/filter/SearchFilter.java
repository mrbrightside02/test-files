package com.mrbri.xml.filter;

import java.io.PrintStream;


import com.mrbri.xml.COMMAND_CONSTANTS;

public class SearchFilter extends FilteredStream {

    private String token;
    private boolean extended_mode = false;

    public SearchFilter(PrintStream out) {
        super(out);
    }

    public void setToken(String token) {
        this.token = token;
    }


    public void filter(String output) {
        if (this.token == null) {
            return;
        } else {
            if (this.matches(output)) {
                this.out.println(output);
            }
        }
    }


    public void setExtended_mode(boolean extended_mode) {
        this.extended_mode = extended_mode;
        this.makeExtendedToken();
    }

    private void makeExtendedToken() {
        this.token = this.token
                .replace("`", "")
                .replace(".", "\\.")
                .replace("*", ".*");
    }

    private boolean matches(String output) {
        String[] tokens = output.split(COMMAND_CONSTANTS.DIRECTORY_SEPARATOR);
        if (!this.extended_mode) {
            return tokens[tokens.length - 1].equals(this.token);
        }
        return tokens[tokens.length - 1].matches(this.token);
    }

}
