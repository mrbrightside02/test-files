package com.mrbri.xml.filter;

import java.io.PrintStream;

public abstract class FilteredStream {
    protected PrintStream out;

    FilteredStream(PrintStream out) {
        this.out = out;
    }

    public abstract void filter(String string);
}
