package com.mrbri;

import com.google.devtools.common.options.OptionsParser;

import com.mrbri.xml.CommandOptions;
import com.mrbri.xml.XMLParser;
import com.mrbri.xml.filter.PlainFilter;
import com.mrbri.xml.filter.RegexFilter;
import com.mrbri.xml.filter.SearchFilter;

import java.util.Collections;


public class Command {

    public static void main(String[] args) {

        OptionsParser parser = OptionsParser.newOptionsParser(CommandOptions.class);
        parser.parseAndExitUponError(args);
        CommandOptions options = parser.getOptions(CommandOptions.class);

        assert options != null;

        if (options.file.isEmpty()) {
            printUsage(parser);
            return;
        }

        if (options.search.isEmpty() && options.searchExtended.isEmpty()) {
            PlainFilter filter = new PlainFilter(System.out);
            new XMLParser(options.file, filter);
        } else if (options.searchExtended.isEmpty()) {
            SearchFilter filter = new SearchFilter(System.out);
            filter.setToken(options.search);
            if (options.search.contains("*")) {
                filter.setExtended_mode(true);
            }
            new XMLParser(options.file, filter);
        } else {
            RegexFilter filter = new RegexFilter(System.out);
            filter.setPattern(options.searchExtended);
            new XMLParser(options.file, filter);
        }

    }

    private static void printUsage(OptionsParser parser) {
        System.out.println("Usage: java -jar ${filename} OPTIONS");
        System.out.println(parser.describeOptions(Collections.<String, String>emptyMap(),
                OptionsParser.HelpVerbosity.LONG));
    }

}

