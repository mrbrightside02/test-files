XML parser demo 
=============================
Build
------------
``` mvn package ```


Compile and run
------------
``` mvn compile && mvn exec:java -Dexec.args="${argument_list}" ```

Options description
-----------
Run Command : ``java -jar target/test_assignment-1.0.jar``